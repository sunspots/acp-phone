import Vue from 'vue'
import App from './App'
	// 引入 api.js 文件
	import { myRequest } from './utils/myRequest.js'
	// 挂载到全局使用
	Vue.prototype.$myRequest = myRequest
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
