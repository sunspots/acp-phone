// 请求服务器地址
const baseURL = 'http://127.0.0.1:9900'
// 向外暴露一个方法 myRequest
export const myRequest = (options) => {
	return new Promise((resolve, reject) => {
		uni.request({
			// 开发者服务器接口地址（请求服务器地址 + 具体接口名）
			url: baseURL + options.url,
			// 请求方式（若不传，则默认为 GET ）
			method: options.method || 'POST',
			// 请求参数（若不传，则默认为 {} ）
			data: options.data || {},
			header:{
				Authorization:"Bearer fe126517-8d61-40dd-bdff-4d53f9e9fd93"
			},
			// 请求成功
			success: (res) => {
				resolve(res);
			},
			// 请求失败
			fail: (err) => {
				uni.showToast({
					title: '请求接口失败！'
				})
				reject(err)
			}
		})
	})
}